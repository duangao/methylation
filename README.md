甲基化生信分析pipeline – 甲基化测序的一键自动生信分析流程
=======================================
## 甲基化基本概念：
[甲基化概念](https://www.thermofisher.cn/cn/zh/home/life-science/epigenetics-noncoding-rna-research/methylation-analysis-.html)
## 下载公共数据库GEO的甲基化测序数据：
[GEO甲基化数据](https://www.ncbi.nlm.nih.gov/gds/?term=methylation>)
## 项目介绍：
#### 本项目开发语言为python加R，运行环境为Linux，用户仅需更改配置文件即可一键运行。
#### 请运行下面命令安装依赖同时需要逐一安装主要依赖中的库：
    conda install -c bioconda bismark
    conda install -c bioconda fastp
## 主要功能：
-    质量控制-fastp
-    甲基化reads比对-bismark
-    甲基化位点提取-DMRfinder
-    样本PCA投影测量距离-FactoMineR
-    甲基化位点Manhattan图-qqman
-    十等分统计甲基化位点检测-自行开发
-    组间差异甲基化分析-DSS
-    甲基化基因的volcano plot-EnhancedVolcano
-    甲基化基因的heatmap plot-pheatmap
-    发送状态代码到API-request
## 主要依赖：
#### python>=3.0.0：
-    os
-    sys
-    re
-    time
-    subprocess
-    requests
-    pathlib
-    pandas
-    numpy
-    shelve
-    math
-    pymysql
-    matplotlib
#### R>=3.6.0:
-    qqman
-    EnhancedVolcano
-    pheatmap
-    FactoMineR
-    factoextra
-    DSS
-    bsseq
## 运行前需更改配置文件config.json内容：
    {"sampth_path":[["/home/DUAN/methyl_rawdata/yinle/J-H-0_1.fq.gz","/home/DUAN/methyl_rawdata/yinle/J-H-0_2.fq.gz"],["/home/DUAN/methyl_rawdata/yinle/J-H-5_1.fq.gz","/home/DUAN/methyl_rawdata/yinle/J-H-5_2.fq.gz"],["/home/DUAN/methyl_rawdata/yinle/293FT_1.fq.gz","/home/DUAN/methyl_rawdata/yinle/293FT_2.fq.gz"],["/home/DUAN/methyl_rawdata/yinle/293FT-HOSK_1.fq.gz","/home/DUAN/methyl_rawdata/yinle/293FT-HOSK_2.fq.gz"],["/home/DUAN/methyl_rawdata/yinle/J-H-10_1.fq.gz","/home/DUAN/methyl_rawdata/yinle/J-H-10_2.fq.gz"]],"control_name":["J-H-0","J-H-5"],"treat_name":["293FT","293FT-HOSK","J-H-10"],"result_path":"/public/mlrna_seq/sample_result/26/","id":"26"} 
### 请将配置文件中的样本替换为自己的，control_name和treat_name分别为实验组和对照组，命名随意但要确保不重名和对应fastq样本数，result_path是存放结果的路径可替换为自己目标路径，id为任务号，请填写正整数数值。
## 运行：
    python methylation.py 
## 部分运行结果展示：
<img src="https://gitee.com/duangao/methylation/raw/test/sample_result/heatmap.png" alt="图片替换文本" width="500" height="500" />
<img src="https://gitee.com/duangao/methylation/raw/test/sample_result/manhattan.png" alt="图片替换文本" width="500" height="500" />
<img src="https://gitee.com/duangao/methylation/raw/test/sample_result/res.png" alt="图片替换文本" width="500" height="500" />
<img src="https://gitee.com/duangao/methylation/raw/test/sample_result/volcano.png" alt="图片替换文本" width="500" height="500" />


    
    


